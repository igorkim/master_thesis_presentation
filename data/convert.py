fname = "data/test.json"
fnameSave = "data/test.js"

f = open(fname)
lines = f.readlines()
output = "plotData = \""

for l in lines:
    output += l.strip().replace(" ", "").replace("\"", "\\\"")

output += "\""

f1 = open(fnameSave, "w")
f1.write(output)

f.close()
f1.close()

