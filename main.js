// More info about config & dependencies:
// - https://github.com/hakimel/reveal.js#configuration
// - https://github.com/hakimel/reveal.js#dependencies
Reveal.initialize({
    dependencies: [
        { src: 'reveal.js/plugin/markdown/marked.js' },
        { src: 'reveal.js/plugin/markdown/markdown.js' },
        { src: 'reveal.js/plugin/zoom-js/zoom.js' },
        { src: 'reveal.js/plugin/notes/notes.js', async: true },
        { src: 'reveal.js/plugin/highlight/highlight.js', async: true, callback: function () { hljs.initHighlightingOnLoad(); } }
    ]
});

// Overwrite key codes for presenter support
Reveal.configure({
    transition: 'convex',
    keyboard: {
        // down
        40: 'right',
        // up 
        38: 'left',
        // right
        39: 'next',
        // left
        37: 'prev',
        // F5
        116: 'togglePause',
        //esc
        27: 'togglePause'
    }
});

// Prevent openning developer console with a wireless presenter
document.onkeypress = function (event) {
    event = (event || window.event);

    if (event.shiftKey) {
        if (event.charCode == 83) {
            return false;
        }
    }
    if (event.keyCode == 116) {
        //alert('No F-5');
        return false;
    }
};

// Controller for plots
class AppController {
    constructor() {
        this.plots = {}
        this.plotData = {}
        this.plotKey = "100"
    }

    initPlot(key, id, title, maxY = 100, minY = 0) {
        this.plots[key] = {
            "name": key,
            "id": id,
            "currentX": 0,
            "maxX": 20,
            "lastX": 0,
            "timer": undefined
        }
        var ctx = document.getElementById(id);
        this.plots[key]["chartObj"] = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    label: title,
                    data: [],
                    borderColor: [
                        'rgba(255,255,255,1)',
                    ],
                }],
                labels: []
            },
            options: {
                animation: false,
                legend: {
                    labels: {
                        fontSize: 18
                    }
                },
                legend: {
                    display: true,
                    labels: {
                        fontColor: '#FFF',
                        fontSize: 16
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "rgba(255,255,255,0.4)",
                            zeroLineColor: "rgba(255,255,255,1)"
                        },
                        ticks: {
                            min: minY,
                            max: maxY,
                            stepSize: 5,
                            fontColor: "#FFF",
                            fontSize: 16
                        }
                    }]
                }
            }
        });
    }

    changePlot(key) {
        this.plotKey = key;

        var x = document.getElementsByClassName("plot-button");
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("selected");
        }
        var element = document.getElementById(key + "_btn");
        if (element !== null)
            element.classList.add("selected");
    }

    removeChartData(key) {
        var len = this.plots[key].chartObj.data.labels.length;

        for (var i = 0; i < len; i++) {
            this.plots[key].chartObj.data.labels.pop();
        }
        this.plots[key].chartObj.data.datasets.forEach((dataset) => {
            for (i = 0; i < len; i++) {
                dataset.data.pop();
            }
        });
        this.plots[key].currentX = 0;
        this.plots[key].chartObj.update();
    }

    loadPlot(key) {
        this.removeChartData(key);

        var len = this.plotData[key][this.plotKey].labels.length;
        for (var i = 0; i < len; i++) {
            this.plots[key].chartObj.data.labels.push(this.plotData[key][this.plotKey].labels[i]);
            this.plots[key].chartObj.data.datasets[0].data.push(this.plotData[key][this.plotKey].data[i]);
        }
        this.plots[key].chartObj.update();
    }
}

function nextPoint(key, isMasterKey) {
    app.plots[key].lastX++;
    app.plots[key].currentX++;

    if (app.plots[key].currentX > app.plots[key].maxX) {
        app.plots[key].chartObj.data.labels.shift(1);
        app.plots[key].chartObj.data.datasets[0].data.shift(1);
    }
    if (app.plots[key].lastX >= app.plotData[key][app.plotKey].labels.length) {
        if (!app.plotData[key][app.plotKey].canLoop && isMasterKey) {
            app.plotKey = app.plotData[key][app.plotKey].nextKey;
        }
        app.plots[key].lastX = 0;
    }
    app.plots[key].chartObj.data.labels.push(app.plotData[key][app.plotKey].labels[app.plots[key].lastX]);
    app.plots[key].chartObj.data.datasets[0].data.push(app.plotData[key][app.plotKey].data[app.plots[key].lastX]);
    app.plots[key].chartObj.update();
}

function startPlot(key, isMasterKey) {
    if (typeof (app.plots[key].timer) == "undefined") {
        app.plots[key].timer = setInterval(function () { nextPoint(key, isMasterKey); }, 1000);
    } else {
        clearInterval(app.plots[key].timer);
        app.plots[key].timer = undefined;
    }
}


function resetPlotX(key) {
    app.plots[key].lastX = 0;
}

// Create controller instance
app = new AppController();
// reveal.js is ready
Reveal.addEventListener('ready', function (event) {
    app.plotData = JSON.parse(plotData);
    app.initPlot("ptime", "myChart", 'Downlink Encoding Processing Time, µs', 70, 150);
    app.initPlot("tp", "myChart1", 'Physical Layer Throughput, MBit/s', 0, 40);

    app.initPlot("ptime1", "myChart2", 'Downlink Encoding Processing Time, µs', 0, 150);
    app.initPlot("mcs", "myChart3", 'Modulation and Conding Scheme', 0, 29);
});

// Listener for slide change events
Reveal.addEventListener('slidechanged', function (event) {
    // event.previousSlide, event.currentSlide, event.indexh, event.indexv
    if (event.indexh == 8) {
        app.plotKey = "noData";

        app.loadPlot("ptime1");
        app.plots["ptime1"].chartObj.options["annotation"] = {
            annotations: [{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                value: 65,
                borderColor: 'rgb(255, 0, 0)',
                borderWidth: 2,
                label: {
                    enabled: false,
                    content: 'Test label'
                }
            },{
                type: 'line',
                mode: 'horizontal',
                scaleID: 'y-axis-0',
                value: 45,
                borderColor: 'rgb(0, 255, 0)',
                borderWidth: 2,
                label: {
                    enabled: false,
                    content: 'Test label'
                }
            }]
        }

        app.loadPlot("mcs");
    } else if (event.indexh == 9) {
        app.plotKey = "100";
        app.loadPlot("ptime");
        app.loadPlot("tp");
    }
});